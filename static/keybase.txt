==================================================================
https://keybase.io/katdotsh
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://sarahkat.net
  * I am katdotsh (https://keybase.io/katdotsh) on keybase.
  * I have a public key ASCTz33yVJ46NZA6oHMGrHYr_mBnKWCZNO3UGZ8ZN8HKGAo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01206dfcf5ae5da634bc7eb3dc112e97de28c4721a3886ad2b63d5790ed91342df250a",
      "host": "keybase.io",
      "kid": "012093cf7df2549e3a35903aa07306ac762bfe606729609934edd4199f1937c1ca180a",
      "uid": "e66cd6f77cdd9f7dc70ceede43164219",
      "username": "katdotsh"
    },
    "merkle_root": {
      "ctime": 1599532845,
      "hash": "a0e1297f3c50ae6a5eae881deca9ac0726099dce858e4495a52cd6506ddf08e9769182f3797ac17ad442a4c822d42925e2e7ec0505422ebc6a0058026207a974",
      "hash_meta": "8833ca1b9fe2d682e2fa72e36d7121bb367c6efb65b68e9606362bbd39143fe1",
      "seqno": 17502631
    },
    "service": {
      "entropy": "nj/2pA+1IODULP33JbsysK9p",
      "hostname": "sarahkat.net",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.5.1"
  },
  "ctime": 1599532951,
  "expire_in": 504576000,
  "prev": "2ce75358f8114f98b25d8be2a747dd85d12bcb86c79ff14e441afe0df30f6021",
  "seqno": 20,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgk8998lSeOjWQOqBzBqx2K/5gZylgmTTt1BmfGTfByhgKp3BheWxvYWTESpcCFMQgLOdTWPgRT5iyXYvip0fdhdEry4bHn/FORBr+DfMPYCHEIMG7Mb7UeDUoP5+ZH+Qy+YfRuPGxNNICH5Rsz9299F9pAgHCo3NpZ8RAv/qTmWQ7EDmqTjZxnNnSLAARX6Iew0B/QNV/GydkxiQJqzw9Gf+5+rASPHf85uuwy4F2fo57llZweXrbF3EvBqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIEvOCDRtVQZcwy0jDPuo8we/PnmOE6PHKH4g8mtDsLeqo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/katdotsh

==================================================================
