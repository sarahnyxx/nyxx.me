	function tmuxtime(){
		var datetime = new Date();
			var h = datetime.getHours();
		var i = datetime.getMinutes();
		var s = datetime.getSeconds();
			var d = datetime.getDate();
		var m = datetime.getMonth();
		switch(m){
			case 0:
				m = "Jan";
				break;
			case 1:
				m = "Feb";
				break;
			case 2:
				m = "Mar";
				break;
			case 3:
				m = "Apr";
				break;
			case 4:
				m = "May";
				break;
			case 5:
				m = "Jun";
				break;
			case 6:
				m = "Jul";
				break;
			case 7:
				m = "Aug";
				break;
			case 8:
				m = "Sep";
				break;
			case 9:
				m = "Oct";
				break;
			case 10:
				m = "Nov";
				break;
			case 11:
				m="Dec";
				break;
		}

		function padTime(t) {
			if ( ( Math.floor(Math.log(t) / Math.LN10 + 1)) == 1) {
				t = "0" + t;
			}
			return t;
		}

		h = padTime(h);
		i = padTime(i);
		s = padTime(s);

		var y = datetime.getFullYear();

		document.getElementById("tmux-time").innerHTML = h + ":" + i + ":" + s + " " + d + "-" + m + "-" + y;
	}
	function bashCursor(){
		$('.bash-cursor').each(function(){
			if ($(this).text() == "_") {
				$(this).html(' ');
			} else if ( $(this).text() == " " ) {
				$(this).html('_');
			}
		});

	}


	$(function(){
		setInterval(tmuxtime, 1000);
		setInterval(bashCursor, 500);
	});

	$(function(){
		$('.bash-prompt').prepend('<span class="bash-green">' + window.location.hostname + '</span>:<span class="bash-blue">/</span> <span class="bash-magenta">$</span> ');
	});
