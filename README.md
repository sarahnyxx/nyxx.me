# SarahKat's site using the static site generator, Hugo! 

[![pipeline status](https://gitlab.com/sarahnyxx/nyxx.me/badges/master/pipeline.svg)](https://gitlab.com/sarahnyxx/nyxx.me/commits/master)

## About this site: 
This website is a portfolio, blog, and wiki for projects and notes that I'm working on. 

I'm working to document these things for my own reference looking back, as well as to help those across the internet. Plus: It makes a great way to show off. 

## How this site is built: 
I'm using this project as a case-study of using Gitlab CI/CD to automatically deploy a website. I was interested in a static site generator simply because I hardly see the need for a full CMS, already love the markdown language, and could use something fairly simple to maintain where I can focus on the writing rather than the site design. 


