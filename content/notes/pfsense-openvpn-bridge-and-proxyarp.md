---
title: "pfSense, OpenVPN Bridge, and ProxyARP"
---
So, I'm not sure why this is the case, but without setting up ProxyARP in the pfSense virtual IP's menu, setting up OpenVPN the way I want does not work. 

The situation: 
I've got a pfSense router running all the routing responsibilities for my home network. I wanted to be able to set up a bridged OpenVPN segment, as to allow clients to attach themselves to the network but still share the same subnet. 

This seemed easy enough - add a new OpenVPN server instance, tell it to run in "tap - Layer 2 Tap Mode", latch it onto "any" interface, give it a distinct port from my other VPN servers, etc. I copied all the cryptographic settings from another existing VPN instance. Then it came to the server configuration. In the end, I manually wrote the server configuration details in the "Custom Options" box as such: 

```
ser ver-bridge 10.13.37.1 255.255.255.0 10.13.37.200  10.13.37.210;

route 10.13.37.0 255.255.255.0;
push "route 10.13.37.0 255.255.255.0";

client-to-client;

duplicate-cn;
```

From here, I saved and started up the VPN. It booted up fine, and then I connected to the VPN on Ubuntu using nmcli. That connected fine. I was able to ping the gateway, get to the internet, but the problem was, I couldn't access anything on the LAN side of the VPN server. 

After some time running tcpdump and checking arp -v, I learned that while the host on the other side of the VPN was receiving packets, it kept asking ARP for a MAC for the IP and not getting a useful response and that it wasn't storing the MAC address and didn't know where to send the reply packet. 

Using this information, I did some research and found that OpenVPN and pfSense does not handle ARP across the bridge well for whichever reason? I would figure it should be able to figure this out by default, but seems that it hasn't.

As a shot in the dark, I went over to the "Firewall > Virtual IPs" page, clicked "Add" and set up a Proxy ARP type on the LAN interface for 10.13.37.0/24 and fed the "Address(es)" field the 10.13.37.0/24 subnet. Save, and suddenly, it worked. 
