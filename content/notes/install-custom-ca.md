---
title: "Install a Custom CA"
---
Why a custom certificate authority? 

With the several home servers that I have, a lot of them try to run self-signed SSL certificates. And each has their own. I'd like to make life a little easier and not have to install each self-signed cert separately, and it slowly becomes a hassle to click "Continue to Site" in the advanced page of Chrome's SSL error page.

To solve this issue, I have started running the certificate authority that I can use to sign certificates that I'm using on home servers and personal projects. The procedure for this will be written up another time. But, by signing each of the server's certificates against the Certificate Authority (CA, for short), and installing the CA's certificate to each of my client machines, all my hosted pages will be trusted by my computer without bypassing any SSL security in Chrome, CURL, etc. 

First, download a .crt file that represents your CA's public key. 

Then, create a folder at /usr/share/ca-certificates/extra

Move your .crt file to this newly created folder

And finally, run dpkg-reconfigure ca-certificates

When running the dpkg, I like to set it to prompt me which certs to enable (So nothing shady slips past my watchful eye), select my CA's certificate, and continue through. 

After this process, the certificate should be installed to the system and should be trusted
