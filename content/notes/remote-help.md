---
title: "Remote Help"
date: 2020-08-27T12:24:50-06:00
draft: true
---

Sometimes, I am asked to help out with a Linux PC or Server. A lot of the times, this is fine because servers usually have SSH port 22 available. They can just give me a user account to hop on with. But what about home PC's, home servers, or anything that's hiding behind a firewall that isn't forwarding SSH port 22? Well, there's reverse SSH tunnels, a no\* software solution to enable a remote administrator's access to a machine behind a firewall. Also especially helpful for a network without a static IP too. I'll be talking in this article in the context of a remote-help situation between an admin, and a client. 

## The Simplest Setup:

So, in theory, all that's needed is: 

1. SSH server running on the client PC, herein referred to as [client-pc]
2. SSH server run by the admin, with SSH port 22 available to the internet, herein referred to as [admin-server]
3. A user account on [admin-server] that the client can log into. 
4. A user account on [client-pc] that admin can log into. 

With those prerequisites fulfilled, the procedure is the following:

1. From the client's PC, simply running `ssh -R 0:*:22 [user]@[admin-server]` gets the reverse session online. 
2. The SSH software on the client's PC will provide a port number in the terminal. Let's call it [client-port]
3. The admin, already logged into [admin-server], runs` ssh [user]@localhost -p [client-port]` using the port provided by the client. 

### Dissecting the SSH commands: 

So, there's two commands relevant here. 

1. `ssh -R 0:*:22 [user]@[admin-server]`
2. `ssh [user]@localhost -p [client-port]`

There are some decisions I'm making that fit my imagined use-case, but might need modification on your end. 

The first command dissects as such: `ssh -R [remote-port]:[remote-bind-address]:[local port]`

The `-R` flag means we're setting this up as a reverse tunnel. 

`[remote-port]` is the port on the remote machine that will be able to reach the client through the reverse tunnel. I'm telling it to use port "0", which will be interpreted as "choose an available port for me" by the server, and will give the port number it chooses to the user. I'm choosing random port assignments because, if ever there is the case of another client PC needing attention, I'd like the same command to work without manually assigning port numbers to different client sessions. It seems better to have it pick a port and provide it to the client. (I could also check for the port on the server without the client's communication, but that's besides the point). 

`[remote-bind-address]` is another case of "I'm setting it as general as I can", the asterisk character I've provided in my example simply sets it to allow all connections from all hosts that can reach [admin-server], with the caveat that additional configuration must be performed to allow logins from anywhere but `localhost`. 

`[port]` just assumes [client-pc] is running SSH on port 22 - it could be any other port and the command would need to be modified to fit the client's setup. 

## A more permanent solution

So, setting this up for regular and easy use is just a couple more steps on the same theory, to remedy a couple of inefficiencies listed in the above procedure. . 

### GatewayPorts

The first thing I would check into doing, is enabling "Gateway Ports" on [admin-server], so that the admin can connect straight into the reverse tunnel from their PC rather than SSH'ing into [admin-server] to then run SSH again on localhost to get onto the tunnel. 

This can be achieved by modifying `/etc/ssh/sshd_config` and appending the line `GatewayPorts yes` to the file, or modifying the line if it exists. 

The security implication is that this can mean that the SSH server on [client-pc] is now available on the internet on [admin-server]'s IP address. There is some security provided by the random port from earlier, but a form of IP whitelisting could be enabled on [admin-server] to provide greater security. 


