---
title: Markdown and Vim
date: 2019-08-08
draft: true
---

# Editing Markdown in Vim

I've found a few settings and tricks for vim to make writing markdown files a tiny bit more pleasant and more like how we'd expect a normal word processor to handle documents. 

## Where to store our settings

To make these changes only apply to markdown files and not to any other type of file, I'm placing these settings in my `~/.vim/ftplugin/markdown.vim` file. 

I've also made an edit to the `~/.vim/ftdetect/markdown.vim` file to include .markdown and .md files. This file looks like this: 

```
au! BufRead,BufNewFile *.markdown set filetype=markdown
au! BufRead,BufNewFile *.md set filetype=markdown
```

Any lines mentioned hereon that start with `:set ` should be assumed to be included in the `ftplugin/mkd.vim` file mentioned. I will include my mkd.vim file at the end and a link to my current configuration as per my dotfiles repo to reflect future changes. 

## Word-Wrap

One of the things we expect a word processor to do, is not cut our words right in the middle to wrap a long line around the edge of the window. Vim allows us to change this with the `:set wrap linebreak nolist` setting. 

## Spellcheck

We can enable a simple spellcheck within VIM with `:set spell spelllang=en_us`. This will check our text against the dictionary for the "en_us" locale. You can change this locale as necessary. 

### Show Spelling Suggestions

To show spelling suggestions, use the key combination `z=` to show a list of suggested words. Type the corresponding number to the word in the list and press enter to substitute the suggestion, or leave empty to keep the word. 

### Adding words to our dictionary

To add a word to our dictionary, use the `zg` key combo while your cursor is over the word in normal mode. In visual mode, this key combo will include any characters selected, including white space. 

## Syntax Highlighting


### Folds
Vim has automatic rules for creating folds in markdown files. These can be found in `/usr/share/vim/vim81/ftplugin/markdown.vim`

Folds created by the automatic syntax rules for markdown can be opened with space, or the key combo `zo`, closed with `zc`, toggled with `za`. 

You can open one level of folds through the whole document with `zr` and `zR` opens all folds in document. 
