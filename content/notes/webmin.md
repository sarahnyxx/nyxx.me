---
title: Uptime Monitoring with Webmin
---

# Server monitoring with Webmin

Getting tired of spending cash on a monitoring solution, and having stumbled through menus to find the ["System and Server Status" module](https://doxfer.webmin.com/Webmin/System_and_Server_Status), I wanted to figure out how to utilize what was available to roll my own solution. This is the story

## Installing webmin

The process is extremely easy, as detailed on webmin's [installation page](http://www.webmin.com/deb.html) for Debian based distributions, like how I'm using Ubuntu for my server installations

The installation is simply: 

* Add the Webmin repository to your system in the `/etc/apt/sources.list` file:

	```
	deb https://download.webmin.com/download/repository sarge contrib
	```
	
* Install the GPG Key to verify the repository: 
	
	```
	wget http://webmin.com/jcameron-key.asc
	apt-key add jcameron-key.asc
	```

* Finally, install: 

	```
	apt install apt-transport-https
	apt update
	apt install webmin
	```
 

## Basic Webmin Configuration

Webmin is pretty much out of the box. It will take your current UNIX user on the system and make a web panel login for you. The web panel will be hosted on the server at [ip addr]:10000 by default, so throw your IP into the browser and tack on port 10000 to the end, and you should be in. 

The Monitoring module we're insterested in is under the "Others" submenu (I've had difficulty finding it before), and that's where we'll go for the rest of the configuration. 

### Setting up basic remote monitoring: 
The System and Server Status module has a lot of options for when you may configure multiple webmin servers to talk to eachother (Detailed later in this document), but let's start out with something easier. 

Webmin offers a few options for monitoring remote services with no configuration on the remote end: 
* Remote FTP
* Remote HTTP / HTTPS
* Remote PING
* Remote SSH
* Remote TCP

Each one of these can be pretty powerful on their own. For instance: 

#### Remote FTP:

Allows you to check connectivity with a remote FTP server. Allows anonymous or authenticated login, TLS support, and the ability to look for a specific file, and a configurable timeout limit. 

#### Remote HTTP / HTTPS

You can check either an HTTP or HTTPS URL using several request methods: HEAD, GET, and POST. You can utilize RegEx matching, which really  opens a lot of possibilites, such as a server side script to print out system stats. You can utilize authentication for the page (Apache .htusers "Basic Authentication"), which can be useful if utilizing the regex if you're serving sensitive information for the monitor. 

#### Remote PING

Self explanatory, and most likely the most simple type to set up. Just give it a hostname / IP and a timeout limit, and it'll ping the system to see if it's up. Of course, requiring ICMP messages through whatever firewall, however. 

#### Remote SSH

Let's you try an SSH login on a remote host. Configured with a hostname and port, and optionally username/password for authentication (Not sure if key access is supported) 

#### Remote TCP

Less configurable than I'd like, but still pleanty powerful. Allowing you to check any sort of arbitrary service, given a hostname and port, with configurable timeout. Would be neat to have RegEx for services that print text out, such as connecting to SMTP over Telnet. 

## Monitoring other Webmin Servers

There are a ton more options for status monitoring given the remote server is running webmin/virtualmin. Such a remote host allows the monitoring service to log into the system and execute commands on the remote host to gather infromation from the host, from average load, free memory, and checking if processes are running. 
  
# TODO: 
* complain about RPC connections
  [Webmin Users, "Can Accept RPC Calls"](https://doxfer.webmin.com/Webmin/Webmin_Users)


