---
title: "ssh Login Banner"
date: 2020-08-27T12:14:20-06:00
---

Why create a login banner? Give users, or strangers, a quick little message before logging into your server. 

To enable the login banner, we will need to edit the ssh-server's config file, `/etc/ssh/sshd_config`, with a line defining where to find the file containing our banner. Add the line `Banner /etc/ssh/sshd_banner` into the config file. 

With that option set to be enabled, then we can create the file `/etc/ssh/sshd_banner` to place our banner contents into. Mine looks like: 

```
 *****     ***    ******     ***    **   **  ***  **  **   **  **   **  **   **
**        ** **   **   **   ** **   **   **  **** **   ** **    ** **    ** ** 
 *****   **   **  ******   **   **  *******  ** ****    ***      ***      ***  
     **  *******  **  **   *******  **   **  **  ***    **      ** **    ** ** 
 *****   **   **  **   **  **   **  **   **  **   **   **      **   **  **   **

+---------------------------------- WARNING ----------------------------------+
|    Unauthorized access to this system is forbidden and will be prosecuted   |
|     by law. By accessing this system, you agree that your actions may be    |
|               monitored if unauthorized usage is suspected.                 |
+-----------------------------------------------------------------------------+
```

With that file written with our banner contents, then we can go ahead and reload the ssh service: `sudo systemctl reload sshd`

Now, when someone connects to the server to SSH, SCP, etc, they will be presented with the banner. It's a fun place to put some art or a warning to those seeking to find easy boxes to mess with. 

One thing of note, is that some find it inadvisable to put much of any host-specific information here - you don't want to give any possible attackers the additional clues they might need to crack your box. There's additionally an "motd", message of the day, file that you could also modify which shows a post-login banner, and this could be a place you put more sensitive information regarding your host. Maybe a large banner that reminds people which box they've connected to? I've accidentally rebooted a few servers in my time. I'm not saying it would of stopped me, but it's an option. an MOTD can be set by modifying the `/etc/motd/` file. 
