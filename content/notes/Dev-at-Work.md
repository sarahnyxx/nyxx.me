---
title: Dev at Work
draft: true
---

The work computers at my place of employment are fairly restrictive, but the workload is nearly nil, so to fight understimulation, I'd like to access my codebase and make changes to it. This is achieved with a few applications installed that don't need admin privileges. 

# git-scm
[git-scm](https://git-scm.com/)

# Git Proxy Settings

Get Current Proxy Settings in Windows: `reg query "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings"`

Look for AutoConfigURL, the proxy settings can be grabbed from the file indicated by the URL. 

Some proxies may require authentication. You can view what permitation of username/password windows is already using for proxy in the control panel under `Control Panel\User Accounts\Credential Manager`. It may not be exactly the proxy credentials listed, but will show the username in plaintext in the correct user and domain settings as required. 

The password may need to have special characters escaped, such as the character '@', so that the proxy server and system know it's part of the password and not part of the greater URL. A handy lookup table exists [here](https://www.w3schools.com/tags/ref_urlencode.ASP)

[Git config](https://gist.github.com/evantoli/f8c23a37eb3558ab8765)
