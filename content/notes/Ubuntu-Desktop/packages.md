---
Title: SarahKat's Debian Packages

---

List of packages to install from the start: 

* vim (full version of vim)
* tmux
* git
* chromium-browser


## Specific to i3 config: 
Manually Compile: 
* [i3-gaps](https://github.com/Airblader/i3)
	* Also required: [xcb-util-xrm](https://github.com/Airblader/xcb-util-xrm)
* [3status](https://github.com/i3/i3status)
* [playerctl](https://github.com/acrisci/playerctl)


Required by i3 config: (Available in Ubuntu Repositories)
* feh
* compton
* xclip
* pulseaudio-utils
* rvxt-unicode
* imagemagick
* arandr


