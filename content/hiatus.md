---
title: Hiatus - Explained
date: 2024-12-15
draft: false
---

So, what's up with the hiatus the past few years? Well, my career took a turn towards some proprietary stuff, that I couldn't really find a way to share while keeping the secrets, well, secret, and with that taking a primary focus, I wound up not finding the time to document the personal projects that I barely had time to maintain. 

However, in hindsight, that leaves a lot of empty time on this blog that I care to keep online, and I would like to fill that temporal emptiness. 

So moving forward, I am hoping to both update new content as well as backdate some old content as I get around to documenting those projects. There definitely are some fun projects such as a deep-dive into amateur radio, getting the homelab on solar power, some tales from defcon and related conferences, as well as new creative pursuits. 

I hope to get some new stuff up here soon, as well as a bit of a reorganization, in hindsight a tagging system would of been so much cooler than the confusing difference between "Journal" and "Notes". Until then, this is my update. Thanks for checking in and I hope to have some fun stories for next time. 