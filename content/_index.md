---
title: "Home"
menu:
  main:
    weight: 10
---

# I'm still new at this blogging thing:
Content may be a bit sparse while I'm getting up to speed. If you've got any tips or resources for blogging, feel free to get in touch. I'm also still working on the theme of the site (Had to be unique and go for a TMUX/Bash themed site).


