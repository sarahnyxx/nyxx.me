---
title: Home Server Project
date: 2019-08-19
lastmod: "2019-08-19"

---

# SRV.HOME - My Home Server Setup

In a way, I've got a mini-datacenter here at home! I use it for learning new software, testing new setups, developing new code, and hosting some of my own cloud services. It's my baby! Basic details below, or, check out the page list here for more details. 

<div class="row">
  <div class="col-md-4">
    <img src="img/homerack-front.jpg" />
    <br />
    <img src="img/homerack-back.jpg" />
    <p>Please excuse the messiness, basements do be like that</p>
  </div>
  <div class="col-md-8">
    <h2>Details:</h2>
    <ul class="list-group" style="list-style: none;">
      <li>Rack: Rosewill 12u 4-post rack</li>
      <li>Dell FY452 - Keyboard Mouse Monitor Rack Console</li>
      <li>Cisco 2960G 24-port Network Switch</li>
      <li>Avocent 3200 KVM Switch</li>
      <li>Dell R610 - Off / Standby</li>
      <li>Dell R710 - Off / Standby</li>
      <li>Dell R720XD - Main Server</li>
      <ul>
        <li>CPU: 2x Intel Xeon E5-2640 @ 3.000GHz</li>
        <li>RAM: 96GB DDR3 ECC</li>
        <li>Data Storage: 6x 2TB HDD (ZFS / RaidZ2)</li>
        <li>OS Storage: 2x500GB (Linux MD, Raid1)</li>
        <li>OS: Ubuntu 20.04.2 LTS</li>
        <li>Hypervisor: QEMU / KVM</li>
        <li>Containers: Docker</li>       
    </ul>
    <li>APC SmartUPS 1500</li>

    <hr />
    <h2>Hosted Services</h2>
    <ul class="list-group" style="list-style: none;">
      <li>Virtual Machines:</li>
      <ul>
        <li>pfSense Router</li>
        <li>Development VM's</li>
      </ul>
      <li>Docker Containers:</li>
      <ul>
        <li>Jellyfin - Media Streaming Server</li>
        <li>NextCloud - Remote File Storage / Sync</li>
        <li>Nginx Proxy Manager - Hostname based matching to backend web services</li>
        <li>Minecraft Servers</li>
      </ul>
    </ul>

  </div>
</div>

