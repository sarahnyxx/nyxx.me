---
title: The Physical Server
draft: true

---

# Design Considerations

When it comes to the physical server, you'll want to pick up something that supports virtualization. In my case, I'm also wanting to use ZFS on my storage, which also has it's own requirements. 

The main requirements for running virtual machines on your system are: 

* A CPU that supports Virtualization, either Intel's VT / VT-X or AMD's AMD-V. 

	Practically any pre-built PowerEdge / ProLiant / etc server aught to support Virtual Machines out of the box, as most of them are running on Intel's server line of CPU's the Xeon, which is meant for this sort of work

* Enough CPU cores and RAM capacity to support the Virtual Machines

	Each virtual machine will require it's own allocated amount of RAM and CPU space, as well as storage. You'd need as much capacity as if you were to add up the RAM requirements of each separate OS and software package.

# Choice of Operating System and Virtual Machine Host

So, when I first set up my home server, I was using vmWare's ESXi operating system - a very bare-bones operating system with only what's needed to give you a console from elsewhere to manage virtual machines. This has the advantage of being ready out-of-the-box and having the absolute minimum maintenance or attack surface of any of the options. The negatives being that it's a fairly locked-down ecosystem. 

I decided to change it up this time around and use a Unix system and KVM for virtualization, as I wanted to use ZFS on the server's storage. This has the advantage of supporting a lot of functionality on the bare-metal server at the cost of necessitating more advanced configuration and requiring more maintenance. 

Alternatively, you could also go with Windows' Hyper-V virtualization solution but this was not my prefered way of doing things, and so I can't say much on it, but this has the same costs as the unix system, but with the added problems of Windows (More malware's written to it, less configurability of underlying system configuration, and Windows Update is annoying) 
