---
Title: SRV.HOME - Drives and Such
---

# So I've been playing around... 

Alright, so the H310 from the Art of Server came through - after figuring out the latch on the motherboard, it was an easy drop-in replacement, and it is tested to be working fine. 

Weird problem, though, is that the disks I have, absolutely refuse to work. 

Here's what I have a few of right now: 

```
Seagate Constellation ES 2TB
Model: ST32000444SS
Firmware: 5C0S
Seagate DKS2C-H2R0SS
```

The H310 in IT mode recognizes just about any other disk I have on hand - From Dell 250GB drives to a WD Red 4TB just fine. It's just that it finds issue with my 6 Seagate drives here. 

# The Issue: 
```
root@ubuntu:/home/ubuntu# lsscsi
[0:0:0:0]	Disk	Seagate	DKS2C-H2R0SS	5C0S	/dev/sda
[0:0:1:0]	Disk	Seagate	DKS2C-H2R0SS	5C0S	/dev/sdb
[0:0:2:0]	Disk	Seagate	DKS2C-H2R0SS	5C0S	/dev/sdc
[0:0:3:0]	Disk	Seagate	DKS2C-H2R0SS	5C0S	/dev/sdd
[0:0:4:0]	Disk	Seagate	DKS2C-H2R0SS	5C0S	/dev/sde
[0:0:5:0]	Disk	Seagate	DKS2C-H2R0SS	5C0S	/dev/sdf

root@ubuntu:/home/ubuntu# ll /dev/sd*
brw-rw---- 1 root disk 8,  0 Jan 7 07:59 /dev/sda
brw-rw---- 1 root disk 8, 16 Jan 7 07:58 /dev/sdb
brw-rw---- 1 root disk 8, 32 Jan 7 07:58 /dev/sdc
brw-rw---- 1 root disk 8, 48 Jan 7 07:58 /dev/sdd
brw-rw---- 1 root disk 8, 64 Jan 7 07:58 /dev/sde
brw-rw---- 1 root disk 8, 80 Jan 7 07:58 /dev/sdf

root@ubuntu:/home/ubuntu# fdisk /dev/sda

Welcome to fdisk (util-linux 2.34)
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command

fdisk: cannot open /dev/sda: No such file or directory

root@ubuntu:/home/ubuntu# zpool create tank radz2 /dev/sda /dev/sdb /dev/sdc /dev/sdd /dev/sde /dev/sdf
cannot label 'sda': try using parted(8) and then provide a specific slice: -4

root@ubuntu:/home/ubuntu# 
```

As seen above, the drives are handled by the card, the driver, presented to the operating system but... not really? They have entries in /dev, they can be queried via smartctl, they show up in `lsscsi`. I'm not sure what's going on with that. It is entirely possible I just found a batch of bad drives, somehow, would explain why I picked em up cheap. The drives at least have spinning spindles - you can feel the gyroscopic procession when removing a drive while spinning, and the SMARTCTL info shows low power-on hours. Really bizzare. I also tried a low-level format from within the H310's boot BIOS, but, that didn't change any behavior. 

I tried these disks in another LSI2008-IT controller and the same issue persists, so I know it's not some weird issue with the Art of Server's cards. 

# Solution! (Update 1) 

Looking a bit deeper into the `dmesg` return, I found: 

```
[ 2462.874892] scsi 0:0:7:0: Direct-Access     SEAGATE  DKS2C-H2R0SS     5C05 PQ: 0 ANSI: 5
[ 2462.874905] scsi 0:0:7:0: SSP: handle(0x0011), sas_addr(0x5000c50034971e55), phy(14), device_name(0x5000c50034971e55)
[ 2462.874908] scsi 0:0:7:0: enclosure logical id (0x500056b36789abff), slot(0) 
[ 2462.875181] scsi 0:0:7:0: Power-on or device reset occurred
[ 2462.877147] sd 0:0:7:0: Attached scsi generic sg6 type 0
[ 2462.877977] sd 0:0:7:0: [sdg] Spinning up disk...
[ 2474.128846] sd 0:0:7:0: [sdg] Unsupported sector size 520.
[ 2474.130229] sd 0:0:7:0: [sdg] 0 512-byte logical blocks: (0 B/0 B)
[ 2474.130231] sd 0:0:7:0: [sdg] 520-byte physical blocks
[ 2474.131145] sd 0:0:7:0: [sdg] Write Protect is off
[ 2474.131153] sd 0:0:7:0: [sdg] Mode Sense: d7 00 10 08
[ 2474.132730] sd 0:0:7:0: [sdg] Write cache: disabled, read cache: enabled, supports DPO and FUA
[ 2474.135776] sd 0:0:7:0: [sdg] Unsupported sector size 520.
[ 2474.140533] sd 0:0:7:0: [sdg] Attached SCSI disk

```

So, the solution is that 520-byte sectors, as used by some vendors to store 8 bytes of proprietary information for disk management, is unsupported by the mpt2sas driver running on the machine. 

Supposedly running a `sg_format --format --size=512 /dev/sda` will reformat the drive to 512 byte sectors? I'm running this now and we'll see how it goes. If it goes well, I'll have to apply it to the rest of my drives. 

```
root@debian:/home/user# sg_format --format --size=512 /dev/sda                            
    SEAGATE   DKS2C-H2R0SS      5C05   peripheral_type: disk [0x0]                        
Mode Sense (block descriptor) data, prior to changes:                                     
  Number of blocks=3824358991 [0xe3f3164f]
  Block size=520 [0x208]

A FORMAT UNIT will commence in 15 seconds
    ALL data on /dev/sda will be DESTROYED
        Press control-C to abort

A FORMAT UNIT will commence in 10 seconds
    ALL data on /dev/sda will be DESTROYED
        Press control-C to abort

A FORMAT UNIT will commence in 5 seconds
    ALL data on /dev/sda will be DESTROYED
        Press control-C to abort

Format unit has started
Format in progress, 0.19% done
Format in progress, 0.42% done
Format in progress, 0.64% done
Format in progress, 0.87% done

```
