---
title: SRV.HOME - Setting up the operating system. 
date: 2020-01-16.md
---

The R720XD has a couple drive bays in the back. These are 2.5" SFF drives. I am populating them with a couple repurposed 500GB Dell drives to fill the role of the operating system storage. This leaves all 12 3.5" LFF bays in the front of the server available for bulk storage seperate from the operating system. 

I want these drives mirrored - while the home server might not be a "mission critical" system, it's good to excersise, learn, and implement the best practices anyway. Besides, it's also a pain to have to set up everything all over again if I don't need to. 

A second goal is to have these drives encrypted. Paranoid? Sure. But it's another skill I'll know to do. If the operating system drives are encrypted, and if I want to encrypt the bulk storage, I can keep the encryption keys for Luks stored on the operating system drives and unlock bulk storage automatically when the operating system's encryption is unlocked. (Luks being the encryption standard popular on linux - like how bitlocker is ubiquitous on Windows). 

Good news, both of these options are built into the installer for Ubuntu Server. I had an issue with my particular version of the Ubuntu Live Server installer for 18.04 LTS (Since I had loaded my boot media, 18.04.3 had come out). I'm using good old fasioned USB boot this time, since I've had to take my previous home server offline in order to scavange drives and parts for the new chassis and network booting as detailed elsewhere on this site is not being hosted. 

So, most of the ubuntu installation is pretty standard, besides one caveat - partitioning of the disks must be done manually, and the order of operations really does matter. Remember, we're putting a filesystem on Luks on md-raid on physical drives, a few layers to go through. 

The server is booting in BIOS mode, so I'm not using a EFI partition. 

My partitioning is as follows: 

* `/dev/sda1` - 1GB - Drive 1's Boot partition - Use as: Volume for RAID
* `/dev/sdb1` - 1GB - Drive 2's Boot Partition - Use as: Volume for RAID
* `/dev/sda2` - MAX - Drive 1's Root Filesystem - Use as: Volume for RAID
* `/dev/sdb2` - MAX - Drive 2's Root Filesystem - Use as: Volume for RAID

With the raw partitions set up, now we can mess with Raid. It is somewhat important to do the RAID before Luks. 

Our boot partition cannot be encrypted, as the GRUB2 bootloader does not have Luks 2 implemented yet. 

* Prepare RAID array - Combine `/dev/sda1` and `/dev/sdb1`, our 1GB /boot partitions, into a RAID 1 array. 
* Prepare RAID array - Combine `/dev/sda2` and `/dev/sdb2`, our /root partitions, into a RAID 1 array. 

Now we can prepare our md raid devices for their next step. 

* `/dev/md0` - Use as /boot, and we're done with that one now. 
* `/dev/md1` - Use as volume for Luks encryption. 

So, we've got /boot figured, now we need to prepare `/dev/md1_crypt` by creating the encryption and unlocking it. Once we have that done, we can then use `/dev/md1_crypt` as the root filesystem. 

* `/dev/md1_crypt` - Use as root, mount point: /

With this configuration, when we boot up our server, it will ask us for the password we defined during installation before getting past the initramfs on the /boot partition, securely locking away any data we have on the server. In my configuration, this will be a couple virtual machines, and the Luks keys for the main storage drives of the system. 
