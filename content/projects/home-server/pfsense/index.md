---
title: PFSENSE.HOME - Home Router
draft: true

---

I'm using pfSense at home in the place of the functionality of a ISP-provided Router or an average consumer WiFi Router. For most use cases, this is a completely optional part of the home server setup, and can be a complicated bit to get working as a virtual machine. 

## Why pfSense

I got started into all this tech stuff by running my own server hosting business, which neccessitated a higher level of configurability than the consumer options provided. As I gained experience in pfSense, I became much more familiar and reliant on it's capabilities. 

Currently, it provides usefulness in having segregated LAN segments (One for desktops/wifi, one for servers, etc), built-in VPN server options, highly-configurable DNS, a handy Certificate Authority management system, and a few others I'm sure I'll get into. 


