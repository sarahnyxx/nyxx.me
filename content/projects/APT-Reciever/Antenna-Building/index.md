---
title: Building the Antenna - Taking Pictures from Space
---

With the quarter wavelength figured out, we can make our fist steps towards construction. We will need to cut 8 lengths from our Copper 8 AWG Wire to this length of 54.7 CM to make our antenna elements

Next, we will want to cut small sections of our wood for support structure. For now, cut off four sections of 5-10 CM each. These will form the mounts for our antenna elements for each dipole. Mark the center of each, and drill a hole through one side of the board. Here, the procedure diverges slightly: 

- If you do not have the terminal blocks, this is enough for this step. 
- If you do have the terminal blocks, mark some holes to pre-drill by placing the terminal blocks close together, but without touching, and marking the mounting hole. Get these holes pre-drilled. 

Prepare some lengths of Coax cable. We will be making four sections, two of which are an arbitrary length, though should be greater than 60 CM, and the second pair will be a quarter wavelength longer than the first pair. So, if the first pair are both 60CM, the second pair would be 60 + 54.7, which comes out to 114.7 CM long. 

Strip your Coax cable on one end, leaving 1 CM of exposed center conductor, leave 1 CM of the dielectric material (The white like-plastic part) while still removing the shield, and have the outer plastic cut 1 CM further down, looking something like this: 

With the Coax cut, we can make each of our four dipoles. 

With one of the small sections of board, attach your copper wire elements

