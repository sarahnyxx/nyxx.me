---
title: "Take Pictures from Space!"
---

# Take Pictures From Space! 
![Satellite Image and Antenna](intro.png)

We grabbed scientific imagery from a variety of satellites in low earth orbit using cheap parts and free software. Space Selfies! 

This repository is currently work in progress, and is missing some details. We are working towards finishing this up. 
## Table of Contents
1. Abstract
2. Materials
3. Methods
	1. Antenna
		1. Antenna Theory & Mathematics
		2. Building the Dipoles
		3. Mounting the Dipoles
		4. Connecting to the Radio
	2. Software
		1. Installing
		2. APT Image Processing for NOAA Satellites
		3. LRPT Image Processing for Meteor M2 Satellite
4. Results
5. Discussion


## Abstract
The purpose of this project is to gather images from weather satellites using low budget materials that can be sourced either around the home or with a trip to the local hardware store. 

## Materials
**Hardware**

- [SDR Reciever](https://www.rtl-sdr.com/buy-rtl-sdr-dvb-t-dongles/). We used a RTL-SDR unit
- SMA to F-Type adapter
- At Least 2.5 Meter length of RG-8 Coax (Any extra will serve as a feed to your SDR)
- At Least 2.5 Meter Length of 8 AWG Wire (Stiff enough to hold shape)
- 3 Meter Length of Wood (Square or Rectangle with small width/depth)
- Duct Tape or Gaffers Tape


**Software**
- SDR Reciever ([SDR#](https://airspy.com/download/) or [GQRX](http://gqrx.dk/), etc)
- [GPredict](http://gpredict.oz9aec.net/)
- [wxtoimg](https://wxtoimgrestored.xyz/)
- [Audacity](https://www.audacityteam.org/)

**Optional**
- Soldering Iron with Solder.
- Heat Shrink.
- F-Type Coupler.
- Ferrite Beads.
	Personally, I scrapped these off of some digital display cables, they can be found in many places, but are not completely necessary. 
- Copper Terminal Blocks, like [these](./resources/materials/copper-block1.jpg), or [these](./resources/materials/copper-block2.jpg). 

## Methods

### The Antenna
The Antenna is a good place to start. Being built is a quad dipole antenna tuned to 137MHz, the approximate frequency of the weather satellites we're trying to reach. 

First, we need to figure out some maths so we can know what we're cutting, and what we're putting where, detailed in ["Antenna Theory and Mathematics"](./Antenna_Theory_and_Mathematics.md). TL;DR: 1/4 Wavelength of 137 MHz is 54.7 CM. 

With the wavelength measurement sorted, then we can get onto [building four dipoles.](./Building_The_Dipoles.md)

The four dipoles are then mounted, at the ends of a cross pattern, and angled 30 degrees.

And then we hook up everything electrically. 

## Results

## Discussion
Better results could be achieved using 50-ohm RG-58 Coax, rather than the 75 ohm RG-8, however this project uses RG-8 as it is in high abundance in most areas, commonly used for cable TV. 


