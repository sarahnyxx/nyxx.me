---
title: Antenna Theory - Taking Pictures from Space
---

What's really important, though, is the wavelength of this frequency, as it will determine the dimensions of our antenna. To calculate the wavelength (In meters), we devide 300 by the Frequency in MHz. 

300 / 137 = 2.189... 

The "300" represents the speed of light, but converted to millions of meters to match with our 137.000 MHz, which is 137 million cycles per second. 

The 2.189 number is not the number we use, however, as we are making half-wavelength dipoles. Dipoles are made from two equal segments of wire, one for the ground plane and one for the signal, both which add up to the half wavelength of 1.095..., which results in a quarter wavelength of: 0.547 Meters

**1/4 Wavelength = 54.7 CM.** This is the number we need. 

