function youtuberss(url) {

	// Check URL for channel_id or user_id
	

	// url like https://youtube.com/channel/some_id
	if ( url.match(/channel/gi) ) {
		url = url.replace('https://www.youtube.com/channel/','');
		url = "https://www.youtube.com/feeds/videos.xml?channel_id=" + url;
	} 
	
	// url like https://youtube.com/user/some_user
	else if ( url.match(/user/gi) ) {
		url = url.replace('https://www.youtube.com/user/','');
		url = "https://www.youtube.com/feeds/videos.xml?user=" + url;
	}

	return url;
}
