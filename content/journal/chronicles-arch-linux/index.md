---
title: "Chronicles in Arch Linux"
date: 2018-12-18
---

As of this installment, I've been using Arch, however sparsely, for two weeks (Installed 2018-12-05, currently writing on 2018-12-18)

## TL;DR as of 2018-12-18:

It's the same shell, I've installed the same window manager and other tools I'd typically set up. Day to day, casual usage is practically the same as any other OS choice I could make. There are some bits under the hood that are different, the community is one of its own, but all in all, it's not too different of an experience for me. 

![Screencap](Screenshot_2018-12-18_13-06-18.png)

This is a change

## What inspired an install: 
The community, mostly

There's quite the cult following for Arch among Linux enthusiasts. From Reddit's /r/unixporn to a number of forums, it seems like it's gained quite the liking from many. The appeal of a more community-based, less corporate-sponsored ecosystem is the main attraction.

## Package Manager:

The package manager for arch comes along with the Arch User Repositories, the AUR, which is a repository of software maintained by the community, more so than most any other distribution out there. The operating system itself operates under the "rolling release", meaning there's no discrete versioning, it's just a matter of keeping packages up to date. This is much reflected in the installation process requiring an internet connection to grab all the packages that make Arch what it is. With the rolling release, and community-maintained repositories, updated software comes a lot sooner, at the risk of being less stable, and there's a wealth of less common packages included in the repositories. From a day-to-day usage standpoint, the only glaring difference is the use of the "pacman" package manager rather than "apt" from debian or "yum" from REHL-derived systems.

## Linux is Linux:

Under the hood, it still runs your usual Linux kernel, though maybe more up to date. (4.19.8 vs 4.4.0-140 on my Ubuntu Servers as of this writing, as a side effect of more unstable, but faster updates). I'm running Bash just like I do on any other system. Since I've gotten into customizing my environment on any OS, I'm running the same i3-gaps window manager too. It's the same x86_64 architecture, compiling from scratch remains much the same (Though fetching dependancies via the package manager is a slightly different experience from other systems). 

## Arch - Build it yourself:

Though, something to be said is that Arch comes pre-installed with nearly nothing to get you going. It takes someone who's already inclined to customize their Linux experience to get nearly anything out of Arch. It is not ready for typical usage out of the box. For me, who'd normally go through the customizations anyway, Arch saves some time by not coming with stuff I'd rather uninstall anyway, but for most, the bare-bones setup out of the box could be rather intimidating.

## My Personal Drawback:

The thing that I'm not getting out of Arch, like I would be getting running Ubuntu, is that I'm not practicing the use of an operating system I'd see out in production. Arch typically would make a terrible server - while the software that comes in the package manager is newer, shinier, and contains more features, it also comes with the risk of more bugs. I, myself, haven't seen any technology stacks in production built on top of Arch. There's a certain utility in using the tools at home that you'd be using in work, and I do find value in that. Arch, while still Linux, doesn't provide the optimal practice in systems I'd be encountering for work. I typically use Ubuntu for my own things, and a lot of folk choose REHL (Red Hat Enterprise Linux) based systems for their production workloads, so moving forward, I may switch off to building my desktop on CentOS, Fedora, or simply sticking with Ubuntu.
