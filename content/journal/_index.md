---
title: "Journal"
menu:
  main:
    weight: 40
---

Journaling about small projects that don't justify their own "Projects" entry, progress updates on projects, or logging tech-related thoughts or ramblings. 
