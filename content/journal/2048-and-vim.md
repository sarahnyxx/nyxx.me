---
title: "2048 and VIM"
date: 2018-10-07
---
First and foremost, I found this excellent version of the game, [2048, written in C.](https://raw.githubusercontent.com/mevdschee/2048.c)

It's fun to have this fun game playable on a Linux terminal - lightweight, no heavy graphics, just the bare essentials game. 

But I noticed something - it's compatible with the VIM movement keys (H, J, K, and L). I'm pretty terrible at using the VIM keys for moving around, I've almost always used the arrow keys for movement. This'd normally be fine, but with the discovery that the up arrow key on my laptop doesn't work worth half the distance I can throw it, I've been finding alternate ways to, well, "cope". Among these, is using the VIM movement keys a LOT more often. 

In 2048, if you're playing the game strategically, you would definitely care about making decisive movements, the tolerance for mistakes is proportional to how seriously you want to take the game. For me, this is a great motivation to get every move right - which requires the decisive use of the VIM keys. 

Playing 2048 with VIM keys is a great way to practice the VIM keys. Especially if your arrow keys are fairly useless. 

This is a tip I've learned for myself and hope it can help someone else out. 

 

Installing:

Installation was really easy for me on Ubuntu. With build-essentials and G++ installed, it was as simple as: 

wget https://raw.githubusercontent.com/mevdschee/2048.c/master/2048.c
gcc -o 2048 2048.c
./2048

And I was playing
