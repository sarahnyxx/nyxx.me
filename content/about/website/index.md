---
title: About This Site

---

This site serves several purposes: 

* To be a portfolio of my projects
* To document projects as tutorials, for others as much as myself, as I can be forgetful about my past problems and solutions. 
* To serve as a living resume - updated as I'm doing new things. 
* To keep web development skills freshly utilized. 

# How This Site's Built

Currently, I am using the [Hugo Static Site Generator](https://gohugo.io/) to build this site. I've previously used plain HTML/CSS and the Concrete5 CMS. 

The design of this current site is built on Bootstrap CSS framework, and is designed to reflect the look and feel of my preferred development environment - TMUX/Bash, though I haven't made it look much like VIM yet. 
