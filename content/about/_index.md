---
title: "About"
menu:
  main:
    weight: 100
---

# Who am I? 

A Dell Latitude C600 from the year 2000. This was my first server. I overwrote the OS with Ubuntu Server and hosted my own website while in middle school. I got tired of the restrictions the website builders had, so I learned HTML, and when they turned off the ability to upload my own HTML pages to their server, I made my own. Over the years, I kept exploring whichever technologies I could learn, found ways to implement them, and kept iterating over this process from my personal projects into my career. 

I started out just trying to push the boundaries for my own sake, but then started hosting websites and game servers for friends, making a business of the affair. Once I was supporting systems for more than just myself, the research into better technologies increased tremendously, culminating to moving my business' servers into a colocation facility and learning how to fit into the datacenter environment. 

These skills transferred into my career - with my first job being an internship and later employment with a local digital agency, where I found many ways to integrate the knowledge gained from my server-hosting business into their business - from implementing virtualization technology and datacenter operations, to custom development for their billing system, and even supporting and expanding the office network through better network technology and VPN access. 

Now I find myself looking for any excuse to push the boundaries of my knowledge by building out more elegant solutions for ioTheory or hacking together personal projects. Right now I find myself extremely interested in cyber security, but still have my hand in systems administration, network operations, and so on. 
